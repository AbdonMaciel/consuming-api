package main

import (
	"fmt"

	"gitlab.com/AbdonMaciel/consuming-api/src/private/modules"
)

func main() {
	fmt.Println("Chamando API...")

	//? Character
	// modules.GetCharacter("10")
	// modules.GetAllCharacter()

	//? Location
	// modules.GetLocation("10")
	// modules.GetAllLocation()

	//? Episode
	// modules.GetEpisode("2")
	modules.GetAllEpisode()

}
