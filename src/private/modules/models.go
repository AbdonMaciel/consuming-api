package modules

type (

	//? Character
	CharacterObj struct {
		Inf Info        `json:"info"`
		Res []Character `json:"results"`
	}

	Info struct {
		Count int    `json:"count"`
		Pages int    `json:"pages"`
		Next  string `json:"next"`
		Prev  string `json:"prev"`
	}
	Character struct {
		Id      int    `json:"id"`
		Name    string `json:"name"`
		Status  string `json:"status"`
		Species string `json:"species"`
		Image   string `json:"image"`
	}

	//? Location
	LocationObj struct {
		Inf Info       `json:"info"`
		Res []Location `json:"results"`
	}

	Location struct {
		Id        int      `json:"id"`
		Name      string   `json:"name"`
		Type      string   `json:"type"`
		Residents []string `json:"residents"`
	}

	//? Episode
	EpisodeObj struct {
		Results []Episode `json:"results"`
	}

	Episode struct {
		Id         int      `json:"id"`
		Name       string   `json:"name"`
		AirDate    string   `json:"air_date"`
		Episode    string   `json:"episode"`
		Characters []string `json:"characters"`
	}
)
