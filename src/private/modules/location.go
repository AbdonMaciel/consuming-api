package modules

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func GetLocation(id string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/location/"+id, nil)
	if err != nil {
		fmt.Println((err.Error()))
	}
	req.Header.Add("Accept", "apllication/json")
	req.Header.Add("Content-type", "apllication/json")

	if err != nil {
		fmt.Println((err.Error()))
	}

	resp, err := client.Do(req)

	if err != nil {
		fmt.Println((err.Error()))
	}

	defer resp.Body.Close()
	bodyBytes, err := io.ReadAll(resp.Body)

	if err != nil {
		fmt.Println((err.Error()))
	}

	var responseObject Location
	json.Unmarshal(bodyBytes, &responseObject)

	fmt.Printf("ID: %v\n", responseObject.Id)
	fmt.Printf("Name: %v\n", responseObject.Name)
	fmt.Printf("Type: %v\n", responseObject.Type)

	for i := 0; i < len(responseObject.Residents); i++ {
		fmt.Printf("Residents: %v\n", responseObject.Residents[i])
	}
}

func GetAllLocation() {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/location", nil)
	if err != nil {
		fmt.Println((err.Error()))
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println((err.Error()))
	}

	defer resp.Body.Close()
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println((err.Error()))
	}

	var responseObject LocationObj
	json.Unmarshal(bodyBytes, &responseObject)

	for i := 0; i < len(responseObject.Res); i++ {
		fmt.Printf("ID: %v\n", responseObject.Res[i].Id)
		fmt.Printf("Name: %v\n", responseObject.Res[i].Name)
		fmt.Printf("Type: %v\n", responseObject.Res[i].Type)

		println("Residents: ", len(responseObject.Res[i].Residents))

		if len(responseObject.Res[i].Residents) > 0 {
			for j := 0; j < len(responseObject.Res[i].Residents); j++ {
				fmt.Printf("Residents: %v\n", responseObject.Res[i].Residents[j])
			}
			fmt.Printf("\n")
		}
	}
}
