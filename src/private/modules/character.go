package modules

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func GetCharacter(id string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/character/"+id, nil)
	if err != nil {
		fmt.Println(err.Error())
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println((err.Error()))
	}

	defer resp.Body.Close()
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println((err.Error()))
	}

	var responseObject Character
	json.Unmarshal(bodyBytes, &responseObject)

	fmt.Printf("Id: %v\n", responseObject.Id)
	fmt.Printf("Name: %s\n", responseObject.Name)
	fmt.Printf("Species: %s\n", responseObject.Species)
	return
}

func GetAllCharacter() {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/character", nil)
	if err != nil {
		fmt.Println((err.Error()))
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println((err.Error()))
	}

	defer resp.Body.Close()
	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println((err.Error()))

	}

	var responseObject CharacterObj
	json.Unmarshal(bodyBytes, &responseObject)

	for i := 0; i < len(responseObject.Res); i++ {
		fmt.Printf("ID: %v\n", responseObject.Res[i].Id)
		fmt.Printf("Name: %v\n", responseObject.Res[i].Name)
		fmt.Printf("Species: %v\n", responseObject.Res[i].Species)
	}

}
