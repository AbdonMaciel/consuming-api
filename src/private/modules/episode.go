package modules

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func GetEpisode(id string) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/episode/"+id, nil)
	if err != nil {
		fmt.Println(err.Error())
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err.Error())
	}

	defer res.Body.Close()

	var responseObject Episode

	bodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err.Error())
	}

	json.Unmarshal(bodyBytes, &responseObject)

	fmt.Printf("Id: %v\n", responseObject.Id)
	fmt.Printf("Name: %v\n", responseObject.Name)
	fmt.Printf("AirDate: %v\n", responseObject.AirDate)
	fmt.Printf("Episode: %v\n", responseObject.Episode)

	for _, v := range responseObject.Characters {
		fmt.Printf("Character: %v\n", v)
	}

}

func GetAllEpisode() {
	client := &http.Client{}

	req, err := http.NewRequest("GET", "https://rickandmortyapi.com/api/episode", nil)
	if err != nil {
		fmt.Println(err.Error())
	}

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err.Error())
	}

	defer res.Body.Close()

	var responseObject EpisodeObj
	bodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err.Error())
	}

	json.Unmarshal(bodyBytes, &responseObject)

	for i := 0; i < len(responseObject.Results); i++ {

		fmt.Printf("Id: %v\n", responseObject.Results[i].Id)
		fmt.Printf("Name: %v\n", responseObject.Results[i].Name)
		fmt.Printf("AirDate: %v\n", responseObject.Results[i].AirDate)
		fmt.Printf("Episode: %v\n", responseObject.Results[i].Episode)

		for _, v := range responseObject.Results[i].Characters {
			fmt.Printf("Character: %v\n", v)
		}
	}

}
